//
//  MapViewModel.swift
//  Week5
//
//  Created by Nguyễn  Mạnh Đức on 9/24/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import GoogleMaps

struct MapViewModel {
    var markerList: Variable<[GMSMarker]> = Variable([])
    var points: Variable<String> = Variable("")
    
    init() {
    }
    
    func addCurrentLocationMaker() -> GMSMarker {
        // Creates default location marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 21.017032, longitude: 105.783795)
        marker.title = "Keangnam Hanoi Landmark Tower"
        marker.snippet = "Hà Nội, Việt Nam"
        markerList.value.append(marker)
        return marker
    }
    
    func addMaker(position: CLLocationCoordinate2D) {
        let marker = GMSMarker()
        marker.position = position
        marker.title = "Destination"
        markerList.value.append(marker)
    }
    
    func showMap() {
        if markerList.value.count < 2 {
            return
        }
        let startMarker = markerList.value[markerList.value.count - 1]
        let destinationMarker = markerList.value[markerList.value.count - 2]
        ApiHelper.instance.getDestination(source: startMarker.position, destination: destinationMarker.position) { (points) in
            self.points.value = points
        }
    }
}

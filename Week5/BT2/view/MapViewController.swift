//
//  MapViewController.swift
//  Week5
//
//  Created by Nguyễn  Mạnh Đức on 9/23/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import RxSwift
import RxCocoa

class MapViewController: UIViewController {
    var mapView: GMSMapView!
    var disposeBag = DisposeBag()
    
    let viewModel = MapViewModel()
    
    override func loadView() {
        // Create a GMSCameraPosition that tells the map to display the
        // me tri: 21.017032, 105.783795
        let camera = GMSCameraPosition.camera(withLatitude: 21.017032, longitude: 105.783795, zoom: 16.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.delegate = self
        view = mapView
        
        viewModel.addCurrentLocationMaker().map = mapView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.markerList.asObservable().subscribe({ (value) in
            guard let array = value.element else {
                return
            }
            array[array.count - 1].map = self.mapView
            
            if array.count > 1 {
                self.viewModel.showMap()
            }
            if array.count > 2 {
                array[array.count - 3].map = nil
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.points.asObservable().subscribe({ (value) in
            guard let data = value.element else {
                return
            }
            let path = GMSPath.init(fromEncodedPath: data)
            let polyline = GMSPolyline(path: path)
            polyline.strokeColor = .blue
            polyline.strokeWidth = 5.0
            polyline.map = self.mapView
        })
        .disposed(by: disposeBag)
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = position
        marker.title = "Destination"
        marker.map = mapView
    }
}

extension MapViewController: GMSMapViewDelegate{
    /* handles Info Window tap */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        viewModel.addMaker(position: coordinate)
    }
    
    /* set a custom Info Window */
//    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 200, height: 70))
//        view.backgroundColor = UIColor.white
//        view.layer.cornerRadius = 6
//
//        let lbl1 = UILabel(frame: CGRect.init(x: 8, y: 8, width: view.frame.size.width - 16, height: 15))
//        lbl1.text = "Hi there!"
//        view.addSubview(lbl1)
//
//        let lbl2 = UILabel(frame: CGRect.init(x: lbl1.frame.origin.x, y: lbl1.frame.origin.y + lbl1.frame.size.height + 3, width: view.frame.size.width - 16, height: 15))
//        lbl2.text = "I am a custom info window."
//        lbl2.font = UIFont.systemFont(ofSize: 14, weight: .light)
//        view.addSubview(lbl2)
//
//        return view
//    }
    
    //MARK - GMSMarker Dragging
}

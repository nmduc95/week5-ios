//
//  ApiHelper.swift
//  Week5
//
//  Created by Nguyễn  Mạnh Đức on 9/24/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import GoogleMaps

class ApiHelper {
    static let instance = ApiHelper()
    
    func getDestination(source: CLLocationCoordinate2D,
                        destination: CLLocationCoordinate2D,
                        completionHandler: @escaping (_ point: String) -> Void) {
        let origin = "\(source.latitude),\(source.longitude)"
        let destination = "\(destination.latitude),\(destination.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=bus&key=AIzaSyDUU4NdnU3IkA633DIFduqkzpZzBM1zQVk"
        
        Alamofire.request(url).responseJSON { response in
            guard let data = response.data else {return}
            do {
                let json = try JSON(data: data)
                let routes = json["routes"].arrayValue
                
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    if let pts = points {
                        completionHandler(pts)
                    }
                }
            } catch {}
        }
    }
}

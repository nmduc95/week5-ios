//
//  ViewController.swift
//  Week5
//
//  Created by Nguyễn  Mạnh Đức on 9/22/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func bt1Btn_pressed(_ sender: Any) {
        if let vc = storyBoard.instantiateViewController(withIdentifier: "BT1ViewController") as? BT1ViewController {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func bt2Btn_pressed(_ sender: Any) {
        if let vc = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as? MapViewController {
            navigationController?.pushViewController(vc, animated: true)
        }
    }

}


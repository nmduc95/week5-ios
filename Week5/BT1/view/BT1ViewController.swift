//
//  BT1ViewController.swift
//  Week5
//
//  Created by Nguyễn  Mạnh Đức on 9/22/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BT1ViewController: UIViewController {
    @IBOutlet weak var increaseBtn: UIButton!
    @IBOutlet weak var decreaseBtn: UIButton!
    @IBOutlet weak var numberLabel: UILabel!
    
    var disposeBag = DisposeBag()
    
    let viewModel = BT1ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberLabel.text = String(viewModel.number.value)
        self.increaseBtn.setTitleColor(UIColor.blue, for: .normal)
        self.decreaseBtn.setTitleColor(UIColor.blue, for: .normal)
        
        viewModel.number.asObservable().subscribe({ (value) in
            guard let num = value.element else {
                return
            }
            self.numberLabel.text = String(num)
            self.decreaseBtn.isEnabled = self.viewModel.isDecreaseValid(number: num)
            self.decreaseBtn.setTitleColor(self.viewModel.decreaseButtonColor(number: num), for: .normal)
        })
            .disposed(by: disposeBag)
        
        increaseBtn.rx.tap.bind {
            self.viewModel.increase()
            }
            .disposed(by: disposeBag)
        
        decreaseBtn.rx.tap.bind {
            self.viewModel.decrease()
            }
            .disposed(by: disposeBag)
    }
}

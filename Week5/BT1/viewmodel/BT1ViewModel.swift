//
//  BT1ViewModel.swift
//  Week5
//
//  Created by Nguyễn  Mạnh Đức on 9/22/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct BT1ViewModel {
    let number = Variable<Int>(2)
    
    init() {
    }
    
    func increase() {
        number.value += 2
    }
    
    func decrease() {
        if number.value <= 0 {
            return
        }
        number.value -= 1
    }
    
    func isDecreaseValid(number: Int) -> Bool {
        return number > 0
    }
    
    func decreaseButtonColor(number: Int) -> UIColor {
        return number > 0 ? UIColor.blue : UIColor.red
    }
}

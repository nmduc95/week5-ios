//
//  BT1ViewModelTest.swift
//  Week5Tests
//
//  Created by Nguyễn  Mạnh Đức on 9/22/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
@testable import Week5

class BT1ViewModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // case 01: number = 0
    func testIsDecreaseValidCase01() {
        let viewModel = BT1ViewModel()
        let expected = false
        let actual = viewModel.isDecreaseValid(number: 0)
        XCTAssertEqual(expected, actual)
    }
    
    // case 02: number > 0
    func testIsDecreaseValidCase02() {
        let viewModel = BT1ViewModel()
        let expected = true
        let actual = viewModel.isDecreaseValid(number: 3)
        XCTAssertEqual(expected, actual)
    }
    
    // case 03: number < 0
    func testIsDecreaseValidCase03() {
        let viewModel = BT1ViewModel()
        let expected = false
        let actual = viewModel.isDecreaseValid(number: -2)
        XCTAssertEqual(expected, actual)
    }
    
    // case 01: number = 0
    func testDecreaseButtonColorCase01() {
        let viewModel = BT1ViewModel()
        let expected = UIColor.red
        let actual = viewModel.decreaseButtonColor(number: 0)
        XCTAssertEqual(expected, actual)
    }
    
    // case 02: number < 0
    func testDecreaseButtonColorCase02() {
        let viewModel = BT1ViewModel()
        let expected = UIColor.red
        let actual = viewModel.decreaseButtonColor(number: -3)
        XCTAssertEqual(expected, actual)
    }
    
    // case 03: number > 0
    func testDecreaseButtonColorCase03() {
        let viewModel = BT1ViewModel()
        let expected = UIColor.blue
        let actual = viewModel.decreaseButtonColor(number: 4)
        XCTAssertEqual(expected, actual)
    }
    
    // case 01: increase number by calling method 1 time
    func testIncreaseCase01() {
        let viewModel = BT1ViewModel()
        let currentNumber = viewModel.number.value
        let expected = currentNumber + 2
        viewModel.increase()
        let actual = viewModel.number.value
        XCTAssertEqual(expected, actual)
    }
    
    // case 02: increase number by calling method 2 times
    func testIncreaseCase02() {
        let viewModel = BT1ViewModel()
        let currentNumber = viewModel.number.value
        let expected = currentNumber + 4
        viewModel.increase()
        viewModel.increase()
        let actual = viewModel.number.value
        XCTAssertEqual(expected, actual)
    }
    
    // case 01: decrease number by calling method 1 time
    func testDecreaseCase01() {
        let viewModel = BT1ViewModel()
        let currentNumber = viewModel.number.value
        let expected = currentNumber - 1
        viewModel.decrease()
        let actual = viewModel.number.value
        XCTAssertEqual(expected, actual)
    }
    
    // case 02: decrease number when value = 0
    func testDecreaseCase02() {
        let viewModel = BT1ViewModel()
        viewModel.number.value = 0
        
        let currentNumber = viewModel.number.value
        let expected = currentNumber
        viewModel.decrease()
        let actual = viewModel.number.value
        XCTAssertEqual(expected, actual)
    }
 }
